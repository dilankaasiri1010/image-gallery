import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from 'app/modules/auth/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let tokenData = this.authService.getToken();
    const domain = request.url.split("/")[2]
    const ignoreDomains = ['dev-pb-apps.s3-eu-west-1.amazonaws.com']

    if (tokenData && tokenData.token && !ignoreDomains.includes(domain)) {
      request = request.clone({
        setHeaders: {
          Authorization: `${tokenData.token}`
        }
      });
    }
    return next.handle(request);
  }
}
