import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const schema = new Schema({
    username: String,
    password: String,
    name: String,
    pictures: [{
        id: Number,
        picture: String,
        sortval: Number,
    }]
});

export default mongoose.model('users', schema);