import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutes } from './auth.routing';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { MaterialModule } from 'app/shared/angular-material/angular-material.module';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './pages/register/register.component';

@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AuthRoutes),

    MaterialModule,
    FormsModule
  ]
})
export class AuthModule { }
