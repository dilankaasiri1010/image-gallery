import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';
import { IAuthModel, IAuthResponseModel } from '../models/auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router,
    private http: HttpClient,
    private snackbar: MatSnackBar) {
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.baseUrl}/api/auth/login`, { username, password })
      .pipe(map(response => {
        const res = response as IAuthResponseModel;
        if (res.data && res.status) {
          localStorage.setItem('tokenInfo', JSON.stringify(res.data));
        }
        return response;
      }));
  }

  logout() {
    localStorage.removeItem('tokenInfo');
    this.router.navigate(['/auth/login']);
  }

  isUserLogged() {
    const storageData = localStorage.getItem('tokenInfo')
    if (!storageData) return false;
    const tokenData = JSON.parse(storageData) as IAuthModel;
    if (tokenData.expiresIn && tokenData.token) return true
    else return false
  }

  getToken() {
    const storageData = localStorage.getItem('tokenInfo')
    if (!storageData) return null;
    const tokenData = JSON.parse(storageData) as IAuthModel;
    if (tokenData.expiresIn && tokenData.token) return tokenData
    else return null
  }
}
