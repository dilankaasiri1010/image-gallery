import { CdkDropListGroup, CdkDropList, moveItemInArray, CdkDrag, CdkDragMove } from '@angular/cdk/drag-drop';
import { ViewportRuler } from '@angular/cdk/overlay';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'environments/environment';
import { IImageAppResponse, IImageApp } from '../../models/image-fetch.model';

@Component({
  selector: 'app-sort-photos',
  templateUrl: './sort-photos.component.html',
  styleUrls: ['./sort-photos.component.css']
})
export class SortPhotosComponent implements OnInit {

  // https://stackblitz.com/edit/angular-dragdrop-grid

  @ViewChild(CdkDropListGroup) listGroup: CdkDropListGroup<CdkDropList>;
  @ViewChild(CdkDropList) placeholder: CdkDropList;

  pictures: IImageApp[] = []

  public target: CdkDropList;
  public targetIndex: number;
  public source: CdkDropList;
  public sourceIndex: number;
  public dragIndex: number;
  public activeContainer;

  constructor(private http: HttpClient,
    private snackbar: MatSnackBar,
    private viewportRuler: ViewportRuler) {
    this.target = null;
    this.source = null;
    this.getImages()
  }

  ngOnInit(): void {
  }

  getImages() {
    this.http.get<any>(`${environment.baseUrl}/api/user/image`).subscribe({
      next: (data: IImageAppResponse) => {
        if (data.data.images) {
          this.pictures = data.data.images
          if (data.data.images.length == 0)
            this.snackbar.open("No images selected", "Okay", {
              duration: 3000
            });
        } else {
          this.snackbar.open("Error! Please try again later", "Okay", {
            duration: 3000
          });
        }
      },
      error: (error) => {
        this.snackbar.open("Error! Please try again later", "Okay", {
          duration: 3000
        });
      },
    });
  }

  filepickerProcessingApi(url: string) {
    const handle = url.split("/").slice(-1)[0]
    return `https://www.filepicker.io/resize=width:250,height:250,fit:crop/quality=value:70/compress/${handle}`
  }

  saveImages() {
    this.pictures.forEach((p, idx) => p.sortval = idx + 1)
    this.http.put<any>(`${environment.baseUrl}/api/user/image`, { images: this.pictures }).subscribe({
      next: (data: IImageAppResponse) => {
        if (data.data.images)
          this.snackbar.open("Saved Successfully!", "Okay", {
            duration: 3000
          });
        else
          this.snackbar.open("Error! Please try again later", "Okay", {
            duration: 3000
          });
      },
      error: (error) => {
        console.log(error)
        this.snackbar.open("Error! Please try again later", "Okay", {
          duration: 3000
        });
      },
    });
  }

  dropListDropped() {
    if (!this.target)
      return;

    let phElement = this.placeholder.element.nativeElement;
    let parent = phElement.parentElement;

    phElement.style.display = 'none';

    parent.removeChild(phElement);
    parent.appendChild(phElement);
    parent.insertBefore(this.source.element.nativeElement, parent.children[this.sourceIndex]);

    this.target = null;
    this.source = null;

    if (this.sourceIndex != this.targetIndex)
      moveItemInArray(this.pictures, this.sourceIndex, this.targetIndex);
  }

  dropListEnterPredicate = (drag: CdkDrag, drop: CdkDropList) => {
    if (drop == this.placeholder)
      return true;

    if (drop != this.activeContainer)
      return false;

    let phElement = this.placeholder.element.nativeElement;
    let sourceElement = drag.dropContainer.element.nativeElement;
    let dropElement = drop.element.nativeElement;

    let dragIndex = __indexOf(dropElement.parentElement.children, (this.source ? phElement : sourceElement));
    let dropIndex = __indexOf(dropElement.parentElement.children, dropElement);

    if (!this.source) {
      this.sourceIndex = dragIndex;
      this.source = drag.dropContainer;

      phElement.style.width = sourceElement.clientWidth + 'px';
      phElement.style.height = sourceElement.clientHeight + 'px';

      sourceElement.parentElement.removeChild(sourceElement);
    }

    this.targetIndex = dropIndex;
    this.target = drop;

    phElement.style.display = '';
    dropElement.parentElement.insertBefore(phElement, (dropIndex > dragIndex
      ? dropElement.nextSibling : dropElement));

    this.placeholder._dropListRef.enter(drag._dragRef, drag.element.nativeElement.offsetLeft, drag.element.nativeElement.offsetTop);
    return false;
  }

  dragMoved(e: CdkDragMove) {
    let point = this.getPointerPositionOnPage(e.event);

    this.listGroup._items.forEach(dropList => {
      if (__isInsideDropListClientRect(dropList, point.x, point.y)) {
        this.activeContainer = dropList;
        return;
      }
    });
  }

  getPointerPositionOnPage(event: MouseEvent | TouchEvent) {
    // `touches` will be empty for start/end events so we have to fall back to `changedTouches`.
    const point = __isTouchEvent(event) ? (event.touches[0] || event.changedTouches[0]) : event;
    const scrollPosition = this.viewportRuler.getViewportScrollPosition();

    return {
      x: point.pageX - scrollPosition.left,
      y: point.pageY - scrollPosition.top
    };
  }

}

function __indexOf(collection, node) {
  return Array.prototype.indexOf.call(collection, node);
};

function __isTouchEvent(event: MouseEvent | TouchEvent): event is TouchEvent {
  return event.type.startsWith('touch');
}

function __isInsideDropListClientRect(dropList: CdkDropList, x: number, y: number) {
  const { top, bottom, left, right } = dropList.element.nativeElement.getBoundingClientRect();
  return y >= top && y <= bottom && x >= left && x <= right;
}
