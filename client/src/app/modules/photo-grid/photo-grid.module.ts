import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from "@angular/cdk/drag-drop";

import { PhotoGridRoutingModule } from './photo-grid-routing.module';
import { PhotoGridComponent } from './pages/photo-grid/photo-grid.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from 'app/shared/angular-material/angular-material.module';
import { SelectPhotosComponent } from './pages/select-photos/select-photos.component';
import { SortPhotosComponent } from './pages/sort-photos/sort-photos.component';

@NgModule({
  declarations: [PhotoGridComponent, SelectPhotosComponent, SortPhotosComponent],
  imports: [
    CommonModule,
    PhotoGridRoutingModule,

    MaterialModule,
    FormsModule,
    DragDropModule
  ]
})
export class PhotoGridModule { }
