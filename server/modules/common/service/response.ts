import { Response } from 'express';

export function successResponse(message: string, data: any, res: Response) {
    return res.status(200).send({
        status: true,
        message: message,
        data
    });
}

export function failureResponse(message: string, data: any, res: Response) {
    return res.status(200).send({
        status: false,
        message: message,
        data
    });
}

export function failureResponseWithStatus(statusCode: number, message: string, data: any, res: Response) {
    return res.status(statusCode).send({
        status: false,
        message: message,
        data
    });
}

export function insufficientParameters(res: Response) {
    return res.status(400).send({
        status: false,
        message: 'Insufficient parameters',
        data: {}
    });
}

export function internalError(err: any, res: Response) {
    return res.status(500).send({
        status: false,
        message: 'Internal error',
        data: err
    });
}