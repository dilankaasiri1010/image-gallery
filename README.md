This application provides a frontend (Angular application) 
and backend rest api for a image selection and sorting solution.

Developed environment;
OS - Windows 10 Pro (20H2)
Node - v14.16.0
MongoDb - v4.4.5

To run the web application,

On the ./server folder run,
npm install
npm run serve

On the ./client folder run,
npm install
ng serve

and then access the web application on,
http://localhost:4200/

To run the tests on the server
On the ./server folder run,
npm run test