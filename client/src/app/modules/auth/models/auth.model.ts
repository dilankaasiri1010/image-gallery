export interface IAuthResponseModel {
  status: boolean;
  data: IAuthModel
}

export interface IAuthModel {
  token: string;
  expiresIn: number;
}