import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'environments/environment';
import { IImageApp, IImageAppResponse } from '../../models/image-fetch.model';

@Component({
  selector: 'app-photo-grid',
  templateUrl: './photo-grid.component.html',
  styleUrls: ['./photo-grid.component.css']
})
export class PhotoGridComponent implements OnInit {

  pictures: IImageApp[] = []

  constructor(private http: HttpClient,
    private snackbar: MatSnackBar) {
    this.getImages()
  }

  ngOnInit(): void {
  }

  getImages() {
    this.http.get<any>(`${environment.baseUrl}/api/user/image`).subscribe({
      next: (data: IImageAppResponse) => {
        if (data.data.images) {
          this.pictures = data.data.images
          if (data.data.images.length == 0)
            this.snackbar.open("No images selected", "Okay", {
              duration: 3000
            });
        } else {
          this.snackbar.open("Error! Please try again later", "Okay", {
            duration: 3000
          });
        }
      },
      error: (error) => {
        this.snackbar.open("Error! Please try again later", "Okay", {
          duration: 3000
        });
      },
    });
  }

  filepickerProcessingApi(url: string) {
    const handle = url.split("/").slice(-1)[0]
    return `https://www.filepicker.io/resize=width:250,height:250,fit:crop/quality=value:70/compress/${handle}`
  }
}
