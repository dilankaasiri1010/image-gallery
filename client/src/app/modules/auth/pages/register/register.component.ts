import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Response } from 'app/shared/models/response.model';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  username: string;
  password: string;
  name: string;
  visible = false;
  inputType = 'password';

  constructor(
    private cd: ChangeDetectorRef,
    private snackbar: MatSnackBar,
    private http: HttpClient,
    private router: Router) { }

  ngOnInit(): void {
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.cd.markForCheck();
    }
  }

  create() {
    if (!(this.username && this.password && this.name)) {
      this.snackbar.open("Invalid Data!", "Okay", {
        duration: 3000
      });
      return
    }

    this.http.post<any>(`${environment.baseUrl}/api/auth/register`, {
      username: this.username,
      password: this.password,
      name: this.name
    }).subscribe({
      next: (data: Response) => {
        this.snackbar.open("User Created Successfully!", "Okay", {
          duration: 3000
        });
        this.router.navigate(["auth/login"]);
      },
      error: (error) => {
        this.snackbar.open("Error! Please try again later", "Okay", {
          duration: 3000
        });
      },
    });

  }

}
