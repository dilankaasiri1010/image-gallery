export interface ISaveImages {
  images: ISaveImage[]
}

export interface ISaveImage {
  id: number;
  picture: string;
  sortval: number;
}