import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'environments/environment';
import { IImageAppResponse, IImageResponse } from '../../models/image-fetch.model';
import { IImageSelected, IIMageView } from '../../models/image.model';

@Component({
  selector: 'app-select-photos',
  templateUrl: './select-photos.component.html',
  styleUrls: ['./select-photos.component.css']
})
export class SelectPhotosComponent implements OnInit {

  pictures: IImageSelected[] = []
  selectedImages: IIMageView[] = []

  constructor(private http: HttpClient, private snackbar: MatSnackBar) {
    this.getImages()
  }

  ngOnInit(): void {
  }

  getImages() {
    this.http.get<any>(environment.imageUrl).subscribe({
      next: (data: IImageResponse) => {
        if (data.entries)
          this.pictures = data.entries.map(i => ({ ...i, selected: false }))
        else
          this.snackbar.open("Error! Please try again later", "Okay", {
            duration: 3000
          });
      },
      error: (error) => {
        this.snackbar.open("Error! Please try again later", "Okay", {
          duration: 3000
        });
      },
    });
  }

  selectImage(image: IImageSelected) {
    if (this.selectedImages.length >= 9) {
      this.snackbar.open("Only 9 images can be selected", "Okay", {
        duration: 3000
      });
      return
    }
    this.pictures.filter(p => p.id == image.id).forEach(p => p.selected = !p.selected)
    if (!image.selected) {
      this.selectedImages = this.selectedImages.filter(p => p.id !== image.id)
    } else {
      this.selectedImages.push({
        id: image.id,
        picture: image.picture
      })
    }
  }

  filepickerProcessingApi(url: string) {
    const handle = url.split("/").slice(-1)[0]
    return `https://www.filepicker.io/resize=width:250,height:250,fit:crop/quality=value:70/compress/${handle}`
  }

  saveImages() {
    if (this.selectedImages.length !== 9) {
      this.snackbar.open("Select 9 images to save", "Okay", {
        duration: 3000
      });
      return;
    }

    this.selectedImages.forEach((i, idx) => i.sortval = idx + 1)
    this.http.put<any>(`${environment.baseUrl}/api/user/image`, { images: this.selectedImages }).subscribe({
      next: (data: IImageAppResponse) => {
        console.log(data)
        if (data.data.images)
          this.snackbar.open("Saved Successfully!", "Okay", {
            duration: 3000
          });
        else
          this.snackbar.open("Error! Please try again later", "Okay", {
            duration: 3000
          });
      },
      error: (error) => {
        console.log(error)
        this.snackbar.open("Error! Please try again later", "Okay", {
          duration: 3000
        });
      },
    });
  }
}
