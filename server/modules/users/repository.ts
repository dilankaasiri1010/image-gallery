import { IUser } from './model';
import users from './schema';

export class UserRepository {

    public createUser(params: IUser, callback: any) {
        const _create = new users(params);
        _create.save(callback);
    }

    public filterUser(query: any, callback: any) {
        users.findOne(query, callback);
    }

    public updateUser(query: any, params: IUser, callback: any) {
        users.findOneAndUpdate(query, params, callback);
    }

}