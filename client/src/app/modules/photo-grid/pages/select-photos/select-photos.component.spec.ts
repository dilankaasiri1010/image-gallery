import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPhotosComponent } from './select-photos.component';

describe('SelectPhotosComponent', () => {
  let component: SelectPhotosComponent;
  let fixture: ComponentFixture<SelectPhotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectPhotosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
