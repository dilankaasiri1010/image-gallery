import express, { Express, Request, Response } from 'express';
import helmet from 'helmet';
import mongoose from 'mongoose'
import cors from 'cors'
import { AuthController } from './api/auth/auth.controller';
import { UserController } from './api/user/user.controller';
class App {

  PORT = 3000;
  app: Express = express();

  private authController: AuthController;
  private userController: UserController;

  constructor() {
    this.authController = new AuthController();
    this.userController = new UserController();
    console.log('Application Started')
    this.dbConnect()
    this.initServer()
    this.configRoutes()
  }

  /**
   * [dbConnect]
   * initialize connection to mongodb
   */
  dbConnect() {
    mongoose.connect('mongodb://localhost:27017/picture-sort', {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    }, () => {
      console.log("connected to db")
    })
  }

  /**
   * [initServer]
   * initialize expressjs server
   */
  initServer() {
    this.app.use(helmet());
    this.app.use(express.json());
    this.app.use(express.urlencoded({
      extended: true
    }));
    this.app.use(cors())
  }

  /**
   * [configRoutes]
   * configure routes
   */
  configRoutes() {
    this.app.get('/', (req: Request, res: Response) => {
      res.send('hi');
    });

    this.app.post('/api/auth/register', (req: Request, res: Response) => {
      this.authController.create(req, res);
    });

    this.app.post('/api/auth/login', (req: Request, res: Response) => {
      this.authController.login(req, res);
    });

    this.app.put('/api/user/image', this.authController.verifyToken, (req: Request, res: Response) => {
      this.userController.saveImages(req, res);
    });

    this.app.get('/api/user/image', this.authController.verifyToken, (req: Request, res: Response) => {
      this.userController.getImages(req, res);
    });

    this.app.listen(this.PORT, () => console.log(`Running on ${this.PORT} ⚡`));
  }
}

const _app = new App();
export default _app;