export interface ICreateUser {
  username: string;
  password: string;
  name: string;
}

export interface IUserLogin {
  username: string;
  password: string;
}