export interface IUser {
    _id?: string;
    username: string;
    password: string;
    name: string;
    pictures: IUserPicture[];
}

interface IUserPicture {
    id: number;
    picture: string;
    sortval: number;
}