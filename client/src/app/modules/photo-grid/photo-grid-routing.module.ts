import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhotoGridComponent } from './pages/photo-grid/photo-grid.component';
import { SelectPhotosComponent } from './pages/select-photos/select-photos.component';
import { SortPhotosComponent } from './pages/sort-photos/sort-photos.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'photo-grid' },
  { path: 'photo-grid', component: PhotoGridComponent },
  { path: 'select-photos', component: SelectPhotosComponent },
  { path: 'sort-photos', component: SortPhotosComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotoGridRoutingModule { }
