export interface IImageSelected {
  id: string;
  picture: string;
  selected: boolean
}

export interface IIMageView {
  id: string;
  picture: string;
  sortval?: number;
}