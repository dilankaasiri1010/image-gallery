import express = require('express');
import { UserService } from '../../modules/users/service';
import * as response from '../../modules/common/service/response'
import { ISaveImage, ISaveImages } from './user.model';

/**
 * [UserController]
 * This controller handles saving user selected, and ordered images
 * and retreving them back
 */
export class UserController {

    private userService: UserService

    constructor() {
        this.userService = new UserService()
    }

    /**
     * [Save Images]
     * This api provides the user with an endpoint
     * to save the selected and ordered images into the db
     */
    async saveImages(req: express.Request, res: express.Response) {
        const reqData = req.body as ISaveImages;

        try {
            // check user
            const user = await this.userService.getUserById(req.userId)
            if (!user) {
                return response.failureResponseWithStatus(404, 'user does not exist', {}, res)
            }

            // save image data against the user
            await this.userService.updateUser(req.userId, { pictures: reqData.images })

            return response.successResponse('user updated', { images: reqData.images }, res)

        } catch (err) {
            response.internalError(err, res)
        }
    }

     /**
     * [Get Images]
     * This api provides the user with an endpoint
     * to get the saved images
     */
    async getImages(req: express.Request, res: express.Response) {
        try {
            // get user data
            const user = await this.userService.getUserById(req.userId)
            if (!user) {
                return response.failureResponseWithStatus(404, 'user does not exist', {}, res)
            }

            const images: ISaveImage[] = [];
            if (user.pictures) {
                user.pictures.forEach(p => images.push({
                    id: p.id,
                    picture: p.picture,
                    sortval: p.sortval
                }))
            }
            return response.successResponse('images retrive successful', { images }, res)

        } catch (err) {
            response.internalError(err, res)
        }
    }
}