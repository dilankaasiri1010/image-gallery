import { IUser } from './model';
import { UserRepository } from './repository';

export class UserService {

  private userService: UserRepository

  constructor() {
    this.userService = new UserRepository()
  }

  public getUserByUsername(username: any): Promise<IUser> {
    return new Promise((resolve, reject) => {
      const query = { username: username };
      this.userService.filterUser(query, (err: any, userData: IUser) => {
        if (err) {
          reject(err);
        } else {
          resolve(userData);
        }
      });
    })
  }

  public getUserById(userId: any): Promise<IUser> {
    return new Promise((resolve, reject) => {
      const query = { _id: userId };
      this.userService.filterUser(query, (err: any, userData: IUser) => {
        if (err) {
          reject(err);
        } else {
          resolve(userData);
        }
      });
    })
  }


  public createUser(user: IUser): Promise<IUser> {
    return new Promise((resolve, reject) => {
      this.userService.createUser(user, (err: any, userData: IUser) => {
        if (err) {
          reject(err);
        } else {
          resolve(userData);
        }
      });
    })
  }

  public updateUser(userId: string, updateObj: any): Promise<IUser> {
    return new Promise((resolve, reject) => {
      const query = { _id: userId };
      this.userService.updateUser(query, updateObj, (err: any, userData: IUser) => {
        if (err) {
          reject(err);
        } else {
          resolve(userData);
        }
      });
    })
  }

}