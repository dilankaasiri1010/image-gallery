import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SortPhotosComponent } from './sort-photos.component';

describe('SortPhotosComponent', () => {
  let component: SortPhotosComponent;
  let fixture: ComponentFixture<SortPhotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SortPhotosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SortPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
