import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  visible = false;
  inputType = 'password';

  isLoggedIn: boolean;

  constructor(
    private cd: ChangeDetectorRef,
    private authService: AuthService,
    private router: Router,
    private snackbar: MatSnackBar
  ) { }

  ngOnInit() { }

  login(): void {
    this.authService.login(this.username, this.password)
      .subscribe(
        data => {
          this.snackbar.open("Login Successful!", "Okay", {
            duration: 3000
          });
          this.router.navigate(['/']);
        },
        error => {
          this.snackbar.open("Invalid Credentials!", "Okay", {
            duration: 3000
          });
        });
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.cd.markForCheck();
    }
  }

}
