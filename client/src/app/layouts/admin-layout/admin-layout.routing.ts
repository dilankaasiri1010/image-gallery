import { Routes } from '@angular/router';

export const AdminLayoutRoutes: Routes = [
    { path: '', loadChildren: '../../modules/photo-grid/photo-grid.module#PhotoGridModule',},
];
