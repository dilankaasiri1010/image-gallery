import { Response } from '../../../shared/models/response.model'

export interface IImageResponse {
  entries: IImage[]
}

export interface IImage {
  id: string;
  picture: string
}

export interface IImageAppResponse extends Response {
  data: {
    images: IImageApp[]
  }
}

export interface IImageApp {
  id: string;
  picture: string
  sortval: number;
}