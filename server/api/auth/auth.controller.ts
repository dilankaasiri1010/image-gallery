import express = require('express');
import { UserService } from '../../modules/users/service';
import { ICreateUser, IUserLogin } from './auth.model';
import * as response from '../../modules/common/service/response'
import { IUser } from '../../modules/users/model';
import bcrypt from 'bcrypt'
import * as jwt from 'jsonwebtoken'

/**
 * [AuthController]
 * This controller handles user registration, login and 
 * and provides a middleware function for protected routes
 */
export class AuthController {

    private userService: UserService
    private secret = "a83b118b0875370129945b71cf28c9c1"
    private expiresIn = 86400

    constructor() {
        this.userService = new UserService()
    }

    /**
     * [Create User]
     */
    async create(req: express.Request, res: express.Response) {
        const reqData = req.body as ICreateUser;

        try {
            // check for existing users with the same username
            const checkUser = await this.userService.getUserByUsername(reqData.username)
            if (checkUser) {
                return response.failureResponse('username already exist', reqData, res)
            }

            // create the hash of password and save in the db
            var hashedPassword = bcrypt.hashSync(req.body.password, 8);
            const newUser: IUser = {
                name: reqData.name,
                password: hashedPassword,
                username: reqData.username,
                pictures: [],
            }

            await this.userService.createUser(newUser)

            return response.successResponse('user created', reqData, res)

        } catch (err) {
            response.internalError(err, res)
        }
    }

    /**
     * [User Login]
     */
    async login(req: express.Request, res: express.Response) {
        const reqData = req.body as IUserLogin;

        try {
            // get user from db
            const user = await this.userService.getUserByUsername(reqData.username)
            if (!user) {
                return response.failureResponseWithStatus(404, 'user does not exist', { username: reqData.username }, res)
            }

            // compare the hash of the provided password with the saved hashed password
            var passwordIsValid = bcrypt.compareSync(reqData.password, user.password);
            if (!passwordIsValid) {
                return response.failureResponseWithStatus(401, 'invalid credentials', { username: reqData.username }, res)
            }

            // generate a new token with userId embedded to it
            var token = jwt.sign({ id: user._id }, this.secret, {
                expiresIn: this.expiresIn
            });
            return response.successResponse('login successful', { token: token, expiresIn: this.expiresIn }, res)

        } catch (err) {
            response.internalError(err, res)
        }
    }

    /**
     * [Token verification middleware]
     */
    verifyToken(req: express.Request, res: express.Response, next: any) {
        // capture authorization header
        var token = req.headers['authorization'] as string;
        if (token.startsWith('Bearer ')) {
            token = token.slice(7, token.length);
        }
        if (!token) {
            return response.failureResponseWithStatus(403, 'No token provided', {}, res)
        }

        // verify token sent in request header
        jwt.verify(token, "a83b118b0875370129945b71cf28c9c1", function (err, decoded: any) {
            if (err || !(decoded && decoded.id))
                return response.failureResponseWithStatus(403, 'Failed to authenticate token', {}, res)

            req.userId = decoded.id;
            next();
        });
    }
}