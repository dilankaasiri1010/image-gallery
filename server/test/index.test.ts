import { expect } from 'chai';

import * as app from '../app';
import { agent as request } from 'supertest';

it('should POST /api/auth/login', async function () {
  const res = await request(app.default.app)
    .post('/api/auth/login').send({ username: 'user', password: 'user' });
  expect(res.status).to.equal(200);
});

describe('useless api endpoint', function() {
  let token = "";
  before(function(done) {
    request(app.default.app)
      .post("/api/auth/login")
      .send({ username: 'user', password: 'user' })
      .end(function(err, res) {
        if (err) throw err;
        const loginData = res.body as any;
        if(loginData.data && loginData.data.token) token = loginData.data.token
        else throw "login failed"
        done();
      });
  });
  
  it('should GET /api/user/image', async function () {
    const res = await request(app.default.app)
    .get('/api/user/image')
    .set({ "Authorization": `Bearer ${token}` });
    expect(res.status).to.equal(200);
    expect(res.body).to.have.property('data')
    expect(res.body.data).to.have.property('images').and.to.be.a('array');
  });
});
